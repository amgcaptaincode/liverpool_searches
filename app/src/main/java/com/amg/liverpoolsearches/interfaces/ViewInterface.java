package com.amg.liverpoolsearches.interfaces;

import com.amg.liverpoolsearches.model.response.LSSearchResults;

public interface ViewInterface {

    void showProducts(LSSearchResults products);

    void withoutResults();

    void showLoading(String status);

    void showError(String message);

}
