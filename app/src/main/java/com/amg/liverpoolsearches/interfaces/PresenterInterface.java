package com.amg.liverpoolsearches.interfaces;

import com.amg.liverpoolsearches.model.request.LSRequest;

public interface PresenterInterface {

    void searchProducts(LSRequest lsRequest);

}
