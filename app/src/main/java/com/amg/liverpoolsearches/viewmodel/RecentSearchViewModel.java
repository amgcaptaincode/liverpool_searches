package com.amg.liverpoolsearches.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.amg.liverpoolsearches.persistence.repository.RecentSearchRepository;
import com.amg.liverpoolsearches.persistence.roomdatabase.entity.RecentSearches;

import java.util.List;

public class RecentSearchViewModel extends AndroidViewModel {

    private RecentSearchRepository mRepository;

    private LiveData<List<RecentSearches>> mRecentSearchAll;

    public RecentSearchViewModel(@NonNull Application application) {
        super(application);

        mRepository = new RecentSearchRepository(application);

        mRecentSearchAll = mRepository.getRecentSearchAll();
    }

    public LiveData<List<RecentSearches>> getRecentSearchAll() {
        return mRecentSearchAll;
    }

    public void insert(RecentSearches recentSearches) {
        mRepository.insert(recentSearches);
    }
}
