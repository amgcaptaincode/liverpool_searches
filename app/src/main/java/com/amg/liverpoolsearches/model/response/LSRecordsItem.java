package com.amg.liverpoolsearches.model.response;

import java.util.List;


import com.google.gson.annotations.SerializedName;

public class LSRecordsItem {

    @SerializedName("isMarketPlace")
    private boolean isMarketPlace;

    @SerializedName("maximumListPrice")
    private double maximumListPrice;

    @SerializedName("groupType")
    private String groupType;

    @SerializedName("productId")
    private String productId;

    @SerializedName("smImage")
    private String smImage;

    @SerializedName("minimumListPrice")
    private double minimumListPrice;

    @SerializedName("xlImage")
    private String xlImage;

    @SerializedName("skuRepositoryId")
    private String skuRepositoryId;

    @SerializedName("productAvgRating")
    private double productAvgRating;

    @SerializedName("marketplaceBTMessage")
    private String marketplaceBTMessage;

    @SerializedName("promoPrice")
    private double promoPrice;

    @SerializedName("minimumPromoPrice")
    private double minimumPromoPrice;

    @SerializedName("productDisplayName")
    private String productDisplayName;

    @SerializedName("productRatingCount")
    private int productRatingCount;

    @SerializedName("isHybrid")
    private boolean isHybrid;

    @SerializedName("variantsColor")
    private List<LSVariantsColorItem> variantsColor;

    @SerializedName("maximumPromoPrice")
    private double maximumPromoPrice;

    @SerializedName("lgImage")
    private String lgImage;

    @SerializedName("productType")
    private String productType;

    @SerializedName("listPrice")
    private double listPrice;

    @SerializedName("marketplaceSLMessage")
    private String marketplaceSLMessage;

    private String location;

    public void setIsMarketPlace(boolean isMarketPlace) {
        this.isMarketPlace = isMarketPlace;
    }

    public boolean isIsMarketPlace() {
        return isMarketPlace;
    }

    public void setMaximumListPrice(double maximumListPrice) {
        this.maximumListPrice = maximumListPrice;
    }

    public double getMaximumListPrice() {
        return maximumListPrice;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setSmImage(String smImage) {
        this.smImage = smImage;
    }

    public String getSmImage() {
        return smImage;
    }

    public void setMinimumListPrice(double minimumListPrice) {
        this.minimumListPrice = minimumListPrice;
    }

    public double getMinimumListPrice() {
        return minimumListPrice;
    }

    public void setXlImage(String xlImage) {
        this.xlImage = xlImage;
    }

    public String getXlImage() {
        return xlImage;
    }

    public void setSkuRepositoryId(String skuRepositoryId) {
        this.skuRepositoryId = skuRepositoryId;
    }

    public String getSkuRepositoryId() {
        return skuRepositoryId;
    }

    public void setProductAvgRating(double productAvgRating) {
        this.productAvgRating = productAvgRating;
    }

    public double getProductAvgRating() {
        return productAvgRating;
    }

    public void setMarketplaceBTMessage(String marketplaceBTMessage) {
        this.marketplaceBTMessage = marketplaceBTMessage;
    }

    public String getMarketplaceBTMessage() {
        return marketplaceBTMessage;
    }

    public void setPromoPrice(double promoPrice) {
        this.promoPrice = promoPrice;
    }

    public double getPromoPrice() {
        return promoPrice;
    }

    public void setMinimumPromoPrice(double minimumPromoPrice) {
        this.minimumPromoPrice = minimumPromoPrice;
    }

    public double getMinimumPromoPrice() {
        return minimumPromoPrice;
    }

    public void setProductDisplayName(String productDisplayName) {
        this.productDisplayName = productDisplayName;
    }

    public String getProductDisplayName() {
        return productDisplayName;
    }

    public void setProductRatingCount(int productRatingCount) {
        this.productRatingCount = productRatingCount;
    }

    public int getProductRatingCount() {
        return productRatingCount;
    }

    public void setIsHybrid(boolean isHybrid) {
        this.isHybrid = isHybrid;
    }

    public boolean isIsHybrid() {
        return isHybrid;
    }

    public void setVariantsColor(List<LSVariantsColorItem> variantsColor) {
        this.variantsColor = variantsColor;
    }

    public List<LSVariantsColorItem> getVariantsColor() {
        return variantsColor;
    }

    public void setMaximumPromoPrice(double maximumPromoPrice) {
        this.maximumPromoPrice = maximumPromoPrice;
    }

    public double getMaximumPromoPrice() {
        return maximumPromoPrice;
    }

    public void setLgImage(String lgImage) {
        this.lgImage = lgImage;
    }

    public String getLgImage() {
        return lgImage;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    public double getListPrice() {
        return listPrice;
    }

    public void setMarketplaceSLMessage(String marketplaceSLMessage) {
        this.marketplaceSLMessage = marketplaceSLMessage;
    }

    public String getMarketplaceSLMessage() {
        return marketplaceSLMessage;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}