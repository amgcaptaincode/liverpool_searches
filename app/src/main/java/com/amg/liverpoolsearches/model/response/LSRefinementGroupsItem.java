package com.amg.liverpoolsearches.model.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class LSRefinementGroupsItem {

    @SerializedName("name")
    private String name;

    @SerializedName("refinement")
    private List<LSRefinementItem> refinement;

    @SerializedName("dimensionName")
    private String dimensionName;

    @SerializedName("multiSelect")
    private boolean multiSelect;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRefinement(List<LSRefinementItem> refinement) {
        this.refinement = refinement;
    }

    public List<LSRefinementItem> getRefinement() {
        return refinement;
    }

    public void setDimensionName(String dimensionName) {
        this.dimensionName = dimensionName;
    }

    public String getDimensionName() {
        return dimensionName;
    }

    public void setMultiSelect(boolean multiSelect) {
        this.multiSelect = multiSelect;
    }

    public boolean isMultiSelect() {
        return multiSelect;
    }
}