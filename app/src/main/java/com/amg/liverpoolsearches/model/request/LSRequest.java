package com.amg.liverpoolsearches.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LSRequest {

    @SerializedName("force-plp")
    @Expose
    private boolean forcePlp;


    @SerializedName("search-string")
    @Expose
    private String searchString;

    @SerializedName("page-number")
    @Expose
    private int pageNumber;

    @SerializedName("number-of-items-per-page")
    @Expose
    private int numberOfItemsPerPage;

    public LSRequest(boolean forcePlp, String searchString, int pageNumber, int numberOfItemsPerPage) {
        this.forcePlp = forcePlp;
        this.searchString = searchString;
        this.pageNumber = pageNumber;
        this.numberOfItemsPerPage = numberOfItemsPerPage;
    }

    public LSRequest(String searchString, int pageNumber, int numberOfItemsPerPage) {
        this.searchString = searchString;
        this.pageNumber = pageNumber;
        this.numberOfItemsPerPage = numberOfItemsPerPage;
    }

    public boolean isForcePlp() {
        return forcePlp;
    }

    public void setForcePlp(boolean forcePlp) {
        this.forcePlp = forcePlp;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getNumberOfItemsPerPage() {
        return numberOfItemsPerPage;
    }

    public void setNumberOfItemsPerPage(int numberOfItemsPerPage) {
        this.numberOfItemsPerPage = numberOfItemsPerPage;
    }
}
