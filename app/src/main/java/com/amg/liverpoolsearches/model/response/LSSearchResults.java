package com.amg.liverpoolsearches.model.response;

import com.google.gson.annotations.SerializedName;

public class LSSearchResults {

    @SerializedName("pageType")
    private String pageType;

    @SerializedName("plpResults")
    private LSPlpResults LSPlpResults;

    @SerializedName("status")
    private LSStatus LSStatus;

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getPageType() {
        return pageType;
    }

    public void setLSPlpResults(LSPlpResults LSPlpResults) {
        this.LSPlpResults = LSPlpResults;
    }

    public LSPlpResults getLSPlpResults() {
        return LSPlpResults;
    }

    public void setLSStatus(LSStatus LSStatus) {
        this.LSStatus = LSStatus;
    }

    public LSStatus getLSStatus() {
        return LSStatus;
    }
}