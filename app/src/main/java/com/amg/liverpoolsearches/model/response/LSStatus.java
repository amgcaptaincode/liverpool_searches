package com.amg.liverpoolsearches.model.response;

import com.google.gson.annotations.SerializedName;

public class LSStatus {

    @SerializedName("status")
    private String status;

    @SerializedName("statusCode")
    private int statusCode;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}