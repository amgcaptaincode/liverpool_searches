package com.amg.liverpoolsearches.model.response;

import com.google.gson.annotations.SerializedName;

public class LSVariantsColorItem {

    @SerializedName("colorName")
    private String colorName;

    @SerializedName("colorImageURL")
    private String colorImageURL;

    @SerializedName("colorHex")
    private String colorHex;

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorImageURL(String colorImageURL) {
        this.colorImageURL = colorImageURL;
    }

    public String getColorImageURL() {
        return colorImageURL;
    }

    public void setColorHex(String colorHex) {
        this.colorHex = colorHex;
    }

    public String getColorHex() {
        return colorHex;
    }
}