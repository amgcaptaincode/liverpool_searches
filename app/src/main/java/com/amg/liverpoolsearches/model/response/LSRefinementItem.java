package com.amg.liverpoolsearches.model.response;

import com.google.gson.annotations.SerializedName;

public class LSRefinementItem {

    @SerializedName("refinementId")
    private String refinementId;

    @SerializedName("count")
    private int count;

    @SerializedName("label")
    private String label;

    @SerializedName("selected")
    private boolean selected;

    public void setRefinementId(String refinementId) {
        this.refinementId = refinementId;
    }

    public String getRefinementId() {
        return refinementId;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }
}