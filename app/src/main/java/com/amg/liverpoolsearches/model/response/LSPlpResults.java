package com.amg.liverpoolsearches.model.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class LSPlpResults {

    @SerializedName("refinementGroups")
    private List<LSRefinementGroupsItem> refinementGroups;

    @SerializedName("records")
    private List<LSRecordsItem> records;

    @SerializedName("label")
    private String label;

    public void setRefinementGroups(List<LSRefinementGroupsItem> refinementGroups) {
        this.refinementGroups = refinementGroups;
    }

    public List<LSRefinementGroupsItem> getRefinementGroups() {
        return refinementGroups;
    }

    public void setRecords(List<LSRecordsItem> records) {
        this.records = records;
    }

    public List<LSRecordsItem> getRecords() {
        return records;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}