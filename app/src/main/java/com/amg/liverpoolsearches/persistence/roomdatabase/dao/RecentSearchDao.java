package com.amg.liverpoolsearches.persistence.roomdatabase.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.amg.liverpoolsearches.persistence.roomdatabase.entity.RecentSearches;

import java.util.List;

@Dao
public interface RecentSearchDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(RecentSearches recentSearches);

    @Query("SELECT * FROM recent_search")
    LiveData<List<RecentSearches>> getAllRecentSearch();

}
