package com.amg.liverpoolsearches.persistence.roomdatabase.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.amg.liverpoolsearches.persistence.roomdatabase.dao.RecentSearchDao;
import com.amg.liverpoolsearches.persistence.roomdatabase.entity.RecentSearches;

@Database(entities = {RecentSearches.class}, version = 1)
public abstract class SearchRoomDatabase extends RoomDatabase {

    public abstract RecentSearchDao recentSearchDao();

    private static volatile SearchRoomDatabase INSTANCE;

    public static SearchRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (SearchRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SearchRoomDatabase.class, "recent_search_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            //new PopulateDbAsync(INSTANCE).execute();
        }
    };


}
