package com.amg.liverpoolsearches.persistence.roomdatabase.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "recent_search", indices = {@Index(value = {"search_string"},
        unique = true)})
public class RecentSearches {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int uuid;

    @ColumnInfo(name = "search_string")
    private String searchString;

    public RecentSearches(String searchString) {
        this.searchString = searchString;
    }

    public int getUuid() {
        return uuid;
    }

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
