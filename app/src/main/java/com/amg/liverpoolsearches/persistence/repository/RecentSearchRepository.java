package com.amg.liverpoolsearches.persistence.repository;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.amg.liverpoolsearches.persistence.roomdatabase.dao.RecentSearchDao;
import com.amg.liverpoolsearches.persistence.roomdatabase.db.SearchRoomDatabase;
import com.amg.liverpoolsearches.persistence.roomdatabase.entity.RecentSearches;

import java.util.List;

public class RecentSearchRepository {

    private RecentSearchDao mRecentSearchDao;
    private LiveData<List<RecentSearches>> mRecentSearchAll;

    public RecentSearchRepository(Application application) {

        SearchRoomDatabase db = SearchRoomDatabase.getDatabase(application);
        mRecentSearchDao = db.recentSearchDao();

        mRecentSearchAll = mRecentSearchDao.getAllRecentSearch();
    }

    public LiveData<List<RecentSearches>> getRecentSearchAll() {
        return mRecentSearchAll;
    }

    public void insert(RecentSearches recentSearches) {
        new insertAsyncTask(mRecentSearchDao).execute(recentSearches);
    }

    @SuppressLint("StaticFieldLeak")
    private class insertAsyncTask extends AsyncTask<RecentSearches, Void, Void> {

        private RecentSearchDao mRecentSearchDao;

        insertAsyncTask(RecentSearchDao recentSearchDao) {
            this.mRecentSearchDao = recentSearchDao;
        }

        @Override
        protected Void doInBackground(final RecentSearches... recentSearches) {
            mRecentSearchDao.insert(recentSearches[0]);
            return null;
        }
    }

}
