package com.amg.liverpoolsearches.network;

import com.amg.liverpoolsearches.model.response.LSSearchResults;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface NetworkInterface {

    @GET("plp?")
    Single<LSSearchResults> searchProducts(@QueryMap Map<String, Object> query);

}
