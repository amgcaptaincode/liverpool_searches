package com.amg.liverpoolsearches.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.amg.liverpoolsearches.interfaces.PresenterInterface;
import com.amg.liverpoolsearches.interfaces.ViewInterface;
import com.amg.liverpoolsearches.model.request.LSRequest;
import com.amg.liverpoolsearches.model.response.LSPlpResults;
import com.amg.liverpoolsearches.model.response.LSRecordsItem;
import com.amg.liverpoolsearches.model.response.LSSearchResults;
import com.amg.liverpoolsearches.network.NetworkClient;
import com.amg.liverpoolsearches.network.NetworkInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class LSPresenter implements PresenterInterface {

    private Context context;
    private ViewInterface mViewInterface;

    public LSPresenter(Context context, ViewInterface mViewInterface) {
        this.context = context;
        this.mViewInterface = mViewInterface;
    }

    @Override
    public void searchProducts(LSRequest lsRequest) {
        mViewInterface.showLoading(String.format("Buscando %s", lsRequest.getSearchString()));

        Map<String, Object> query = new HashMap<>();

        query.put("force-plp", true);
        query.put("search-string", lsRequest.getSearchString());
        query.put("page-number", lsRequest.getPageNumber());
        query.put("number-of-items-per-page", lsRequest.getNumberOfItemsPerPage());

        getObservableData(query).subscribeWith(getObserverData());
    }

    private Single<LSSearchResults> getObservableData(Map<String, Object> requestData) {
        return NetworkClient.getRetrofit()
                .create(NetworkInterface.class)
                .searchProducts(requestData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DisposableSingleObserver getObserverData() {
        return new DisposableSingleObserver<LSSearchResults>() {
            @Override
            public void onSuccess(LSSearchResults lsSearchResults) {
                showProducts(lsSearchResults);
            }

            @Override
            public void onError(Throwable e) {
                mViewInterface.showError(messageError(e));
            }
        };
    }

    private void showProducts(LSSearchResults lsSearchResults) {
        if (lsSearchResults != null) {
            List<LSRecordsItem> items = lsSearchResults.getLSPlpResults().getRecords();
            if (items != null && items.size() > 0) {
                mViewInterface.showProducts(lsSearchResults);
            } else {
                mViewInterface.withoutResults();
            }
        } else {
            mViewInterface.withoutResults();
        }
    }

    private String messageError(Throwable e) {
        String message = "";
        try {
            if (e instanceof IOException) {
                message = "Sin conexión a internet.";
            } else if (e instanceof HttpException) {
                HttpException error = (HttpException) e;
                String errorBody = error.response().errorBody().string();
                JsonObject jObj = new Gson().fromJson(errorBody, JsonObject.class);

                message = jObj.get("error").getAsString();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        if (TextUtils.isEmpty(message)) {
            message = "Error desconocido";
        }
        return message;
    }
}
