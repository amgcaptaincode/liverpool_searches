package com.amg.liverpoolsearches.view.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.amg.liverpoolsearches.R;
import com.amg.liverpoolsearches.interfaces.ViewInterface;
import com.amg.liverpoolsearches.manager.Toolbox;
import com.amg.liverpoolsearches.model.request.LSRequest;
import com.amg.liverpoolsearches.model.response.LSSearchResults;
import com.amg.liverpoolsearches.persistence.roomdatabase.entity.RecentSearches;
import com.amg.liverpoolsearches.presenter.LSPresenter;
import com.amg.liverpoolsearches.view.adapters.ProductsAdapter;
import com.amg.liverpoolsearches.viewmodel.RecentSearchViewModel;

import java.util.ArrayList;
import java.util.List;

public class LSMainActivity extends AppCompatActivity implements ViewInterface {

    private ProductsAdapter productsAdapter;
    private Context mContext;
    private SearchView searchViewProducts;
    private RecyclerView rvProducts;
    private TextView tvWithoutResults;

    private LSPresenter mPresenter;

    private AlertDialog alertDialog = null;

    private RecentSearchViewModel mRecentSearchViewModel;
    private SimpleCursorAdapter mSimpleCursorAdapter;
    private List<RecentSearches> mRecentSearches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        initListeners();
    }

    private void initViews() {
        mContext = getApplicationContext();

        mRecentSearchViewModel = ViewModelProviders.of(this).get(RecentSearchViewModel.class);

        mPresenter = new LSPresenter(mContext, this);
        mRecentSearches = new ArrayList<>();

        searchViewProducts = findViewById(R.id.search_view_products);
        rvProducts = findViewById(R.id.rv_products);
        tvWithoutResults = findViewById(R.id.tv_without_results);

        rvProducts.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));

        productsAdapter = new ProductsAdapter(mContext);

        rvProducts.setAdapter(productsAdapter);

        //mPresenter.searchProducts(new LSRequest("macbook", 1, 20));
        final String[] from = new String[]{"recent_products"};
        final int[] to = new int[]{android.R.id.text1};
        mSimpleCursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        searchViewProducts.setSuggestionsAdapter(mSimpleCursorAdapter);

    }

    private void initListeners() {
        searchViewProducts.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!query.equals("") && !query.trim().equals("")) {
                    mRecentSearchViewModel.insert(new RecentSearches(query));
                    mPresenter.searchProducts(new LSRequest(query, 1, 20));
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchViewProducts.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                Cursor cursor = (Cursor) mSimpleCursorAdapter.getItem(position);
                String txt = cursor.getString(cursor.getColumnIndex("recent_products"));
                searchViewProducts.setQuery(txt, true);
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = (Cursor) mSimpleCursorAdapter.getItem(position);
                String txt = cursor.getString(cursor.getColumnIndex("recent_products"));
                searchViewProducts.setQuery(txt, true);
                return true;
            }
        });

        mRecentSearchViewModel.getRecentSearchAll().observe(this,
                new Observer<List<RecentSearches>>() {
                    @Override
                    public void onChanged(List<RecentSearches> recentSearches) {
                        if (mRecentSearches != null) {
                            mRecentSearches.clear();
                            mRecentSearches = new ArrayList<>();
                            mRecentSearches.addAll(recentSearches);
                        }
                        populateAdapter(recentSearches);
                    }
                });
    }

    @Override
    public void showProducts(LSSearchResults products) {
        Toolbox.hideSoftKeyboard(this);
        rvProducts.setVisibility(View.VISIBLE);
        tvWithoutResults.setVisibility(View.GONE);
        productsAdapter.setProducts(products.getLSPlpResults().getRecords());
        alertDialog.dismiss();
    }

    @Override
    public void withoutResults() {
        Toolbox.hideSoftKeyboard(this);
        rvProducts.setVisibility(View.GONE);
        tvWithoutResults.setVisibility(View.VISIBLE);
        alertDialog.dismiss();
    }

    @Override
    public void showLoading(String status) {
        Toolbox.hideSoftKeyboard(this);
        showDialogStatus(status);
    }

    @Override
    public void showError(String message) {
        alertDialog.dismiss();
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }


    private void showDialogStatus(final String status) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.ls_status_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(view);

        TextView tvStatus = view.findViewById(R.id.tv_status);
        ProgressBar progressBar = view.findViewById(R.id.progress_bar);

        tvStatus.setText(status);

        alertDialogBuilder
                .setCancelable(false);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void populateAdapter(List<RecentSearches> recentSearches) {
        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "recent_products"});
        for (int i = 0; i < recentSearches.size(); i++) {
            c.addRow(new Object[]{i, recentSearches.get(i).getSearchString()});
        }
        mSimpleCursorAdapter.changeCursor(c);
    }
}
