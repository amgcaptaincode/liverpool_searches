package com.amg.liverpoolsearches.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amg.liverpoolsearches.R;
import com.amg.liverpoolsearches.manager.Toolbox;
import com.amg.liverpoolsearches.model.response.LSRecordsItem;
import com.bumptech.glide.Glide;

import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private List<LSRecordsItem> mProducts;
    private Context context;

    public ProductsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.ls_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mProducts == null ? 0 : mProducts.size();
    }

    public void setProducts(List<LSRecordsItem> products) {
        this.mProducts = products;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivThumbnail;
        private TextView tvTitle, tvLocation, tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivThumbnail = itemView.findViewById(R.id.iv_thumbnail);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvLocation = itemView.findViewById(R.id.tv_location);
            tvPrice = itemView.findViewById(R.id.tv_price);

        }

        public void onBind(int position) {

            LSRecordsItem product = mProducts.get(position);

            tvTitle.setText(product.getProductDisplayName());
            tvLocation.setText(product.getProductId());

            tvPrice.setText(String.format("$ %s", Toolbox.formatPrice(product.getListPrice())));

            Glide.with(itemView.getContext())
                    .load(product.getSmImage())
                    .into(ivThumbnail);

        }

    }
}
